var productList = {

};
var cart = new Cart();
//функция нахождения в массиве
function findInArray(ar, val) {
    for (var i = 0; i < ar.length; i++) {
        if (ar[i] === val) {
            return i;
        }
    }
    return -1;
}
//класс книжного слайдера
function BookSlider(items) {
    this.items = items;
}
BookSlider.prototype.render = function () {
    var wrapper = document.querySelector(".sliderWrapper");
    var renderResult = '';
    renderResult += '<div class="bookSlider"><div class="bookSlider__button bookSlider__button-left"></div><div class="bookSlider__button bookSlider__button-right"></div><div class="bookSlider__content">';
    for (var i = 0; i < this.items.length; i++) {
        //если айтем - Book, вызываем у него метот render
        if (this.items[i] instanceof Book) {
            // console.log(this.items[i]);
            renderResult += this.items[i].render();
        }
    }

    renderResult += '</div></div>';
    wrapper.innerHTML += renderResult;
};


//класс отдельной книги
function Book(last, first, typeDiscount, name, discount, picture, price, isActive, index, calculatedDiscount, calculatedSalePrice) {
    this.lastName = last;
    this.firstName = first;
    this.typeDiscount = typeDiscount;
    this.name = name;
    this.discount = discount;
    this.picture = picture;
    this.price = price;
    this.isActive = isActive;
    this.index = index;
    this.calculatedDiscount = calculatedDiscount;
    this.calculatedSalePrice = calculatedSalePrice;
}
//все книги в идут в каталог книг (массив productList)
Book.prototype.addToCatalog = function (id) {
    productList[this.index] = [this.name, this.price, this.calculatedSalePrice, this.calculatedDiscount, this.index];
};
//рендер карточки книги
Book.prototype.render = function (i) {
    var renderResultBook = '';
    var discSumm;
    if (this.calculatedDiscount > 0) {
        discSumm = this.calculatedDiscount + ' р.';
    } else {
        discSumm = '';
    }
    renderResultBook = '<div class="book" id="' + this.index + '"><a href="#" class="book__link"><img src="' + this.picture + '" class="book__cover" title="' + this.name + '" alt="' + this.name + '"></a><a href="#" class="book__link"><div class="book__title">' + this.name + '</div><div class="book__author">' + this.lastName + ' ' + this.firstName + '</div></a><div class="book__priceWrapper"><div><p class="book__price">' + this.price + ' р.</p><p class="book__price-discount">' + discSumm + ' </p></div><button class="button button__addToCart" onclick="cart.add(' + this.index + ')" id="b' + this.index + '">В корзину</button></div></div>';
    this.addToCatalog(); //во время рендера запускаем метод добавления книги в каталог
    return renderResultBook;
};
//класс корзины
function Cart() {
    this.itemsInCart = [];
    this.fullPrice = 0;
    this.fullDiscount = 0;
    //метод положить/вынуть из карзины
    this.add = function (id) {
        var bookID = String(id); // ID карточки книжки
        var buttonID = 'b' + bookID // ID кнопки карточки

        var bookSlide = document.getElementById(bookID);
        bookSlide.classList.toggle('book-added');
        var bookButton = document.getElementById(buttonID);
        bookButton.classList.toggle('button__addToCart');
        bookButton.classList.toggle('button__addToCart-added');
        if (bookButton.innerHTML == 'В корзину') {
            bookButton.innerHTML = 'Из корзины';
        } else {
            bookButton.innerHTML = 'В корзину';
        }
        //проверка на то, в корзине ли объект. Если нет - добавляем, калькулируем. Если да - удаляем из корзины, калькулируем
        var testArray = findInArray(this.itemsInCart, id);
        if (testArray < 0) {
            this.itemsInCart.push(id);
            this.calculate();
        } else {
            this.itemsInCart.splice(testArray, 1);
            this.calculate();
        }
        //теперь есть смысл зарендерить таблицу с товарами
        this.render();
    };

    //метод рендера
    this.render = function () {

        var cartWrapper = document.querySelector(".cart");
        var isEmpty = true;
        var renderResult = '';
        if (this.itemsInCart.length > 0) {
            isEmpty = false;
            renderResult += '<table class="inCart"><tr class="inCart__header"><th class="inCart__header-left">Название</th><th class="inCart__header-right">Цена с учетом скидки</th></tr>';


            for (var i = 0; i < this.itemsInCart.length; i++) {

                renderResult += '<tr><td class="inCart__name">' + productList[this.itemsInCart[i]][0] + '<button class="button__removeFromCart" onclick="cart.add(' + productList[this.itemsInCart[i]][4] + ')" title="Удалить из корзины">X</button></td><td class="inCart__price">' + productList[this.itemsInCart[i]][2] + ' руб.</td></tr>';
            }

            var commulativePrice = parseFloat(this.fullDiscount) + parseFloat(this.fullPrice);
            commulativePrice = (commulativePrice).toFixed(2);
            renderResult += '<tr class="inCart__footer"><td>Итого:</td><td class="incart__footer">Всего <span id="cartPrice-withDisc">' + this.fullPrice + '</span> руб.</td></tr><tr class="inCart__footer"><td></td><td>Скидка <span id="cartPrice-disc">' + this.fullDiscount + '</span> руб.</td></tr><tr class="inCart__footer"><td></td><td class="incart__footer">Без скидки <span id="cartPrice-withoutDisc">' + commulativePrice + '</span> руб.</td></tr></table>';
            cartWrapper.innerHTML = renderResult;
        } else {
            isEmpty = true;
            cartWrapper.innerHTML = '';
        }


    };



    //метод высчитывания подытогов
    this.calculate = function () {
        this.fullPrice = 0;
        var subTotalPrice = 0;
        this.fullDiscount = 0;
        var subTotalDiscount = 0;
        for (var i = 0; i < this.itemsInCart.length; i++) {
            subTotalPrice += productList[this.itemsInCart[i]][2];
            subTotalDiscount += productList[this.itemsInCart[i]][3];
        }
        subTotalPrice = (subTotalPrice).toFixed(2);
        subTotalDiscount = (subTotalDiscount).toFixed(2);
        this.fullPrice = subTotalPrice;
        this.fullDiscount = subTotalDiscount;

    };


}

//реквест для запроса данных
var xhr = new XMLHttpRequest();
xhr.open('GET', './test.json', true);
xhr.onreadystatechange = function () {
    if (xhr.readyState !== 4) {
        return;
    }
    if (xhr.status !== 200) {
        console.log('Не удалось получить список книг!');
        return;
    }
    //парсим JSON в массив данных;
    var myItemsArray = JSON.parse(xhr.responseText);
    var items = []; //Для объектов класса Book
    var calcDiscount = 0; //Скалькулированная сумма скидки
    var calcSalePrice = 0; //Скалькулированная стоимость книги с учетом высчитанной скидки

    //если книга неактивна, то НЕ создаем объект Book
    for (var i = 0; i < myItemsArray.length; i++) {
        if (myItemsArray[i].isActive) {
            //высчитываем скидку
            if (myItemsArray[i].typediscount === 'P') {
                var fullPriceFloat = parseFloat(myItemsArray[i].price);
                calcDiscount = (fullPriceFloat * myItemsArray[i].discount / 100).toFixed(2);
                calcDiscount = parseFloat(calcDiscount);
            } else {
                var fullPriceFloat = parseFloat(myItemsArray[i].price);
                calcDiscount = myItemsArray[i].discount;

            }
            //высчитываем цену со скидкой
            calcSalePrice = (fullPriceFloat - calcDiscount).toFixed(2);
            calcSalePrice = parseFloat(calcSalePrice);

            //пушим в массив items свежесозданные объекты класса Book 
            items.push(new Book(myItemsArray[i].author.last, myItemsArray[i].author.first, myItemsArray[i].typediscount, myItemsArray[i].name, myItemsArray[i].discount, myItemsArray[i].picture, fullPriceFloat, myItemsArray[i].isActive, myItemsArray[i].index, calcDiscount, calcSalePrice));
            // console.log('book was added to slider');
        }
    }
    //создаю объект класса BookSlider, помещая туда данные массива items
    var slider = new BookSlider(items);
    slider.render();
};
xhr.send();


// window.onload
function initiate () {
    
    var bookWidth = 260; // Ширина карточки книги в пикселах + отступы (240 + 10 + 10)
    var bookList = document.querySelectorAll(".book"); // Находим карточки книг
    var currentSliderIndex = 0; // индекс первого слайда
    var maximumSliderIndex = bookList.length - 3; // индекс последнего слайда
    var rangeEnd = 0;
    var additionalItems = 2;
    setTimeout(sliderIsLoaded(), 500);

    function sliderIsLoaded() {
        var bookList = document.querySelectorAll(".book"); // Находим карточки книг
        // расставляем элементы с индексом от 0 до 2, а остальные скрываем
        for (var i = 0; i <= bookList.length - 1; i++) {
            if (i < 3) {
                transform(i, i * bookWidth);
                bookList[i].classList.toggle("active");
            } else {
                transform(i, bookWidth * 3);
                toggleInactive(i);
            }
        }

    

    }
// навешиваем на кнопки обработчик клика
        var buttonLeft = document.querySelector(".bookSlider__button-left");
        var buttonRight = document.querySelector(".bookSlider__button-right");
  
    
                buttonLeft.addEventListener("click", function () {
                    scrollRight();
                });

                buttonRight.addEventListener("click", function () {
                    scrollLeft();
                });
          
       

    // служебные функции
    function transform(index, value) {
        bookList[index].style.transform = "translateX(" + value + "px)";
    }

    function toggleClass(index) {
        bookList[index].classList.toggle("active");
        toggleInactive(index);
    }

    function toggleInactive(index) {
        bookList[index].classList.toggle("inactive");
    }

    function scrollLeft() {
        if (currentSliderIndex < maximumSliderIndex) {
            for (var i = 0, a = 0 - bookWidth; i < 3; i++) {
                transform(currentSliderIndex + i, a);
                if (i == 0) {
                    toggleClass(currentSliderIndex);
                }
                a += bookWidth;
            }
            currentSliderIndex += 1;
            rangeEnd = currentSliderIndex + additionalItems;
            transform(rangeEnd, bookWidth * 2);
            toggleClass(rangeEnd);
        } else {
            return false;
        }

    }

    function scrollRight() {
        if (currentSliderIndex > 0) {
            for (var i = 0, a = bookWidth; i < 3; i++) {
                transform(currentSliderIndex + i, a);
                if (i == 2) {

                    toggleClass(currentSliderIndex + i);
                    transform(currentSliderIndex + i, a)
                }
                a += bookWidth;
            }
            currentSliderIndex -= 1;
            rangeEnd = currentSliderIndex;
            transform(rangeEnd, 0);
            toggleClass(rangeEnd);
        } else {
            return false;
        }
    }
}

window.onload = setTimeout(initiate, 100);